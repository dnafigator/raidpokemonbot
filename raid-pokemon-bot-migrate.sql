ALTER TABLE users ADD COLUMN level int(11);

CREATE TABLE `gyms` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(250) NOT NULL,
  `lat` double NOT NULL,
  `lon` double NOT NULL,
  `lat_str` varchar(11) NOT NULL,
  `lon_str` varchar(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name_lat_str_lon_str` (`name`,`lat_str`,`lon_str`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


CREATE TABLE `user_chat` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(10) unsigned,
  `chat_id` bigint(20),
  `chat_name` varchar(100) NOT NULL,
  `post_enabled` BOOLEAN,
  PRIMARY KEY (`id`),
  UNIQUE KEY `user_id_chat_id` (`user_id`,`chat_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


ALTER TABLE attendance ADD notification_time DATETIME DEFAULT CURRENT_TIMESTAMP;
ALTER TABLE attendance ALTER arrived SET DEFAULT 0, ALTER raid_done SET DEFAULT 0, ALTER cancel SET DEFAULT 0;
ALTER TABLE attendance ADD last_message_id VARCHAR(250);
