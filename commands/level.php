<?php

	$level = (int)trim(substr($update['message']['text'],6));

	$level = $level > 40 ? 40 : $level;

	debug_log('SET player level to '.$level);

	$query = 'UPDATE users SET level='.$level.' WHERE user_id='.$update['message']['from']['id'].' ORDER BY id DESC LIMIT 1';
	my_query($query);
	
	sendMessage('none',$update['message']['chat']['id'],'Player level set to '.$level);
	exit;
