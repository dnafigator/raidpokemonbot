<?php
	$gym_data = trim(substr($update['message']['text'],5));

	$data = explode('=',$gym_data,2);
	debug_log('TIME_DATA='.$gym_data);
	
	$err = false;
	if (count($data)<2) {
		$err = true;
	} else {
		$dt = strtotime( $data[1] );
		if ($dt) {
			$dt2 = date('Y-m-d H:i:s', $dt);
			$dt -= 60*60;
			$dt = date('Y-m-d H:i:s', $dt);
		} else {
			$err = true;
		}
	}

	if($err) {
		send_message('none',$update['message']['chat']['id'],'Использование: /time RAID_ID=ВРЕМЯ_ОКОНЧАНИЯ_РЕЙДА
ВРЕМЯ_ОКОНЧАНИЯ_РЕЙДА, например, '.date('Y-m-d H:i'), []);
		exit;
	}

	$q = 'UPDATE raids SET end_time="'.$dt.'" WHERE id='.$db->real_escape_string($data[0]);
	$rs = my_query($q);
	if ( $rs) {
		$text = 'Время окончания рейда '.$data[0].' изменёно на '.$dt2;
	} else {
		$text = 'Что-то пошло не так';
	}

	if ($update['message']['chat']['type']=='private' || $update['callback_query']['message']['chat']['type']=='private') {
		send_message('none',$update['message']['chat']['id'],$text, []);
	} else {
		$reply_to = $update['message']['chat']['id'];
		if ($update['message']['reply_to_message']['message_id']) $reply_to = $update['message']['reply_to_message']['message_id'];
		send_message('none',$update['message']['chat']['id'], $text, [], ['reply_to_message_id'=>$reply_to]);
	}
exit;
