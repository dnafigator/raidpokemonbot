<?php
	$gym_data = trim(substr($update['message']['text'],5));

	$data = explode('=',$gym_data,2);
	debug_log('BOSS_DATA='.$gym_data);

	if (count($data)<2) {
		send_message('none',$update['message']['chat']['id'],'Использование: /boss RAID_ID=POKEMON_NAME',[]);
		exit;
	}

	$q = 'UPDATE raids SET 
		pokemon="'.$db->real_escape_string($data[1]).'"
		WHERE id='.$db->real_escape_string($data[0]).'
	';

	$rs = my_query($q);
	if ( $rs) {
		$text = 'Покемон в рейде '.$data[0].' изменён на '.$data[1];
	} else {
		$text = 'Что-то пошло не так';
	}

	if ($update['message']['chat']['type']=='private' || $update['callback_query']['message']['chat']['type']=='private') {
		send_message('none',$update['message']['chat']['id'],$text, []);
	} else {
		$reply_to = $update['message']['chat']['id'];
		if ($update['message']['reply_to_message']['message_id']) $reply_to = $update['message']['reply_to_message']['message_id'];
		send_message('none',$update['message']['chat']['id'], $text, [], ['reply_to_message_id'=>$reply_to]);
	}
exit;
