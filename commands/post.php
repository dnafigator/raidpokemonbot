<?php
    
    $userId = $update['message']['from']['id'];
    $chatId = $update['message']['chat']['id'];
    $chatName = $update['message']['chat']['title'];

    $q = sprintf('INSERT INTO `user_chat` (`user_id`, `chat_id`, `chat_name`, `post_enabled`) VALUES (%d, %d, "%s", TRUE) ON DUPLICATE KEY update chat_name="%s"',
        $userId,
        $chatId,
        $chatName,
        $chatName
    );
    my_query($q);

	sendMessage('none',$userId,sprintf(t('Chat "%s" added to send list. Type /chats to manage.'), $chatName));
