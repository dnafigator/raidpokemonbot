<?php

if ($update['message']['chat']['type']=='private' || $update['callback_query']['message']['chat']['type']=='private') {
    $userId = $update['callback_query']['from']['id'];
    $q = sprintf ( "UPDATE user_chat set post_enabled=%d WHERE user_id=%d AND chat_id=%d",
        'dis' == $data['arg'] ? 0 : 1,
        $userId,
        $data['id']);
    my_query($q);
    edit_message($update, t('Chat list'), getChatManageKeys($userId), false);
}
