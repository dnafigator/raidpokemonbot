<?php

	raid_access_check($update, $data);

	debug_log('raid_edit()');
	debug_log($update);

	$id = $data['id'];

	$keys = [];
	if (preg_match('/type_(.)/', $data['arg'], $m)) {
		$pokemon = fetchBosses();
		if ( sizeof($m) && isset($pokemon[$m[1]])) {
			$pokNames = $pokemon[$m[1]];
			$line = [];
			foreach( $pokNames as $pokName ) {
				if ( '\n' == $pokName) {
					$keys[] = $line;
					$line = [];
					continue;
				}
				$line[] = [ 'text' => $pokName, 'callback_data' => $id.':edit_poke:'.$pokName ];
			}
			if(sizeof($line)) {
				$keys[] = $line;
			}
		}
	} else {
		/* Edit pokemon */
		$keys = raid_edit_start_keys();
	}

	if (!$keys) $keys = [[[ 'text' => 'Not supported', 'callback_data' => 'edit:not_supported' ]]];

	if (isset($update['callback_query']['inline_message_id'])) {
		editMessageText($update['callback_query']['inline_message_id'],'Choose Raid Boss:',$keys);
	} else {
		editMessageText($update['callback_query']['message']['message_id'],'Choose Raid Boss',$keys,$update['callback_query']['message']['chat']['id'],$keys);
	}
	
	//edit_message_keyboard($update['callback_query']['message']['message_id'],$keys,$update['callback_query']['message']['chat']['id']);
	$callback_response = 'Ok';
	answerCallbackQuery($update['callback_query']['id'],$callback_response);

