<?php
	if(!preg_match('/^(.+)\n/', $update['message']['text'], $m)){
		send_message('none',$update['message']['chat']['id'],'Bad share');
		exit();
	}
	$gymName = $m[1];

	if(!preg_match('/Level: (.+)\n/', $update['message']['text'], $m)){
		send_message('none',$update['message']['chat']['id'],'Bad share');
		exit();
	}
	$bossLevel = (int)$m[1];
	$pokemon = fetchBosses();
	$bossName = '';
	if(!preg_match('/Boss: (.+)\n/', $update['message']['text'], $m)){
		$bosses = $pokemon[''.$bossLevel];
		if(isset($bosses) && 1 == sizeof($bosses)) {
			$bossName = $bosses[0];
		} else {
			$bossName = $bossLevel.'*';
		}
	} else {
		$bossName = $m[1];
	}

	if(!preg_match('/https:\/\/map.poketrack.xyz\/\?lat=([0-9\.]+)&lon=([0-9\.]+)&/', $update['message']['text'], $m)){
		send_message('none',$update['message']['chat']['id'],'Bad share');
		exit();
	}
	$lat = $m[1];
	$lon = $m[2];

	$startTime = false;
	if(!preg_match('/Ends at: (([0-9]+[0-9]+):([0-9]+[0-9]+) (AM|PM))/', $update['message']['text'], $m)){
		if(!preg_match('/Starts at: (([0-9]+[0-9]+):([0-9]+[0-9]+) (AM|PM))/', $update['message']['text'], $m)){
			send_message('none',$update['message']['chat']['id'],'Bad share');
			exit();
		} else {
			$startTime = true;
		}
	}
	$time = $m[1];
	
	$tz = get_timezone($lat, $lon);
	$addr = get_address($lat, $lon);
	if ($tz===false) {
		$tz = 'Europe/Moscow';
	}
		
	$endTime = new DateTime($time, new DateTimeZone($tz));
	if($startTime) {
		$endTime->add(new DateInterval('PT45M'));
	}


	$insert = true;
	$precision = .0001;
	$q = 'SELECT * FROM gyms WHERE'.
	'(	  lat > '.($lat*(1-$precision)).
	' AND lat < '.($lat*(1+$precision)).
	' AND lon > '.($lon*(1-$precision)).
	' AND lon < '.($lon*(1+$precision)).
	' AND name = "'.$gymName.'")'.
	' OR (lat_str = "'.$lat.'" AND lon_str= "'.$lon.'")';
	$rs = my_query($q);
	debug_log('Searching for gym named '.$gymName.' at '.$lat.', '.$lon);
	while($gym = $rs->fetch_assoc()) {
		$gymName = $gym['name'];
		debug_log('Gym named '.$gymName.' at '.$lat.', '.$lon.' found');
		$insert = false;
	}

	if($insert) {
		debug_log('Adding gym named '.$gymName);
		$q = "INSERT IGNORE INTO gyms SET 
				name = '$gymName',
				lat = $lat,
				lon = $lon,
				lat_str = '$lat',
				lon_str = '$lon'";
		$rs = my_query($q);
	}	
	

	$q = 'INSERT INTO raids SET 
		user_id='.$update['message']['from']['id'].', 
		lat="'.$lat.'", 
		lon="'.$lon.'",
		gym_name="'.$gymName.'",
		pokemon="'.$bossName.'",
		end_time="'.$endTime->format('Y-m-d H:i:s').'",
		timezone="'.$tz.'",
		first_seen=NOW()
	';

	if ($addr) {
		$q .= ', address="'.$db->real_escape_string($addr).'"';
	}
	
	$rs = my_query($q);
	$id = my_insert_id();
	debug_log('ID='.$id);

	$keys = [];
	$userId = $update['message']['from']['id'];
	$keys = getShareKeyboard($userId, $id);
	send_message('none',$update['message']['chat']['id'], t('Raid created'), $keys);
	exit();
