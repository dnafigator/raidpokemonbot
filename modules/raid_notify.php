<?php



$clearOld = 'SELECT
    id,
    last_message_id
FROM
    attendance
WHERE
    UNIX_TIMESTAMP(notification_time) - UNIX_TIMESTAMP(NOW()) > 1800
AND 
    last_message_id <> \'error\'
';

$res = my_query($clearOld);
if($res) {
    while($att = $res->fetch_assoc()) {
        if ($att['last_message_id']) {
            list($chat, $message) = explode(':', $att['last_message_id']);
            if($chat && $message) {
                deleteMessage($chat, $message);
            }
        }
        $updateLast = 'UPDATE attendance SET last_message_id = NULL WHERE id='.$att['id'];
        my_query($updateLast);
    }
}


$notificationAfterEnd = 60*10;
$q = 'SELECT
    a.id as id,
    a.user_id as user_id,
    a.raid_id as raid_id,
    r.end_time as end_time,
    a.attend_time as attend_time,
    r.gym_name as gym_name,
    r.pokemon as pokemon,
    a.notification_time as notification_time,
    UNIX_TIMESTAMP(if(a.attend_time IS NULL, r.end_time, a.attend_time)) as deadline,
    UNIX_TIMESTAMP(if(a.attend_time IS NULL, r.end_time, a.attend_time)) - UNIX_TIMESTAMP(NOW()) as time_remains,
    a.last_message_id as last_message_id,
    a.arrived as arrived,
    a.raid_done as raid_done
FROM attendance a
INNER JOIN raids r ON
r.id=a.raid_id AND r.end_time >= DATE_SUB(NOW(), INTERVAL '.$notificationAfterEnd.' SECOND)
WHERE
    a.cancel<>1 AND a.raid_done<>1 AND a.arrived<>1
    AND notification_time <= NOW()
    AND (last_message_id IS NULL OR last_message_id <> \'error\')';

$notificationTimes = [
     31*60,
     11*60,
     6*60,
     0*60,
     -5*60,
     -7*60,
     -9*60,
];

$workTime = 10000000;
$res = my_query($q);
if($res) {
    while($att = $res->fetch_assoc()) {
        //var_dump($att);
        $showKeyboard = true;
        $keys = [
            [['text'=>t('I\'m late for 5 min'),'callback_data'=>$att['raid_id'].':raid_postpone:300' ]],
            [['text'=>t('Won\'t come'),'callback_data'=>$att['raid_id'].':vote_cancel:0' ]],
            [['text'=>t('Already here!'),'callback_data'=>$att['raid_id'].':vote_arrived:0' ]],
            [['text'=>t('I\'m done!'),'callback_data'=>$att['raid_id'].':vote_done:0' ]],
        ];
        $keysMarkup = [
            'reply_markup' => [
                'one_time_keyboard'=>true
            ]
        ];

        if($att['time_remains'] <= 0) {
            if($att['arrived'] != '1') {
                $text = sprintf(t('You\'re late for the raid %s at "%s"'), $att['pokemon'], $att['gym_name']);
            } else {
                $text = sprintf(t('Are you done with the raid %s at "%s"'), $att['pokemon'], $att['gym_name']);
                $keys = [
                    [['text'=>t('Won\'t come'),'callback_data'=>$att['raid_id'].':vote_cancel:0' ]],
                    [['text'=>t('I\'m done!'),'callback_data'=>$att['raid_id'].':vote_done:0' ]],
                ];
            }
        } else {
            $text = sprintf(t('Raid %s at "%s" awaits you in %d min'), $att['pokemon'], $att['gym_name'], $att['time_remains']/60);
        }

        $response = send_message( 'none', $att['user_id'], $text, $keys, $keysMarkup );

        if ($att['last_message_id']) {
            list($chat, $message) = explode(':', $att['last_message_id']);
            if($chat && $message) {
                deleteMessage($chat, $message);
            }
        }
        if($response && $response['ok']) {
            my_query('UPDATE attendance SET last_message_id=\''.$response['result']['chat']['id'].':'.$response['result']['message_id'].'\' WHERE id='.$att['id']);
            foreach($notificationTimes as $notificationTime) {
                if($notificationTime < $att['time_remains']) {
                    $time = $att['deadline']-$notificationTime;
                    $q2 = 'UPDATE attendance SET notification_time=FROM_UNIXTIME('.$time.') WHERE id='.$att['id'];
                    my_query($q2);
                    break;
                }
            }
        } else {
            my_query('UPDATE attendance SET last_message_id=\'error\' WHERE id='.$att['id']);
        }


        if(microtime(true) - $start > $workTime) {
            debug_log('notify timeout', 'RN>');
            die;
        }
    }
}