<?php

if ($update['message']['chat']['type']=='private' || $update['callback_query']['message']['chat']['type']=='private') {
    $userId = $update['callback_query']['from']['id'];
    $raidId = $data['id'];
    $chatId = $data['arg'];
    postRaidToChat($raidId, $chatId);
    answerCallbackQuery($update['callback_query']['id'],t('Raid has been published'));
}
