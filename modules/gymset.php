<?php

	raid_access_check($update, $data);

	debug_log('gymset()');
	debug_log($update);

	$id = $data['id'];
	$arg = $data['arg'];
	$q = 'SELECT * FROM gyms WHERE id='.$arg.' LIMIT 1';
	$callback_response = 'Gym unchanged';
	if ( -1 != $arg ) {
		if( $rs = my_query($q)) {
			if($gym = $rs->fetch_assoc()) {
				$q = "UPDATE raids SET
					lat = '".$gym['lat_str']."',
					lon = '".$gym['lon_str']."',
					gym_name = '".$gym['name']."'
					WHERE id=".$id;
				my_query($q);
				$callback_response = 'Gym set to '.$gym['name'];
			}
		}
	}
	
	$keys = raid_edit_start_keys($id);
	edit_message($update, 'Choose Raid level:', $keys);
	answerCallbackQuery($update['callback_query']['id'],$callback_response);
	
	exit();

