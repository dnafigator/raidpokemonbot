<?php

	define('CR', "\n");
	define('CR2', "\n");

	define (BOSS_TIME, 45);
	define (EGG_TIME, 60);

//	define('TEAM_B', "&#x1f499;");
//	define('TEAM_R', "&#x2764;");
//	define('TEAM_Y', "&#x1f49B;");
//	define('TEAM_B', "\ud83d\udc99");
//	define('TEAM_R', "\u2764\ufe0f");
//	define('TEAM_Y', "\ud83d\udc9b");
//	define('TEAM_CANCEL', "\ud83d\udc94");
//	define('TEAM_DONE', "\ud83d\udc98");

	define('TEAM_B', iconv('UCS-4LE', 'UTF-8', pack('V', 0x1f9B5)));
	define('TEAM_R', iconv('UCS-4LE', 'UTF-8', pack('V', 0x1f4ba)));
	define('TEAM_Y', iconv('UCS-4LE', 'UTF-8', pack('V', 0x1f681)));
	define('TEAM_CANCEL', iconv('UCS-4LE', 'UTF-8', pack('V', 0x1f494)));
	define('TEAM_DONE', iconv('UCS-4LE', 'UTF-8', pack('V', 0x1f498)));
	define('TEAM_UNKNOWN', iconv('UCS-4LE', 'UTF-8', pack('V', 0x1F5A4)));

	define('EMOJI_REFRESH', iconv('UCS-4LE', 'UTF-8', pack('V', 0x1F504)));
//	define('EMOJI_EGG', iconv('UCS-4LE', 'UTF-8', pack('V', 0x1F95A))); // not showing on TG web
	define('EMOJI_EGG', iconv('UCS-4LE', 'UTF-8', pack('V', 0x1F95A)));//0x1F373
	define('EMOJI_BOSS', iconv('UCS-4LE', 'UTF-8', pack('V', 0x1F608)));

	define('EMOJI_FEET', iconv('UCS-4LE', 'UTF-8', pack('V', 0x1F463)));
	define('EMOJI_FACE', iconv('UCS-4LE', 'UTF-8', pack('V', 0x1F466)));
	define('EMOJI_TICK', iconv('UCS-4LE', 'UTF-8', pack('V', 0x2705)));
	define('EMOJI_CROSS', iconv('UCS-4LE', 'UTF-8', pack('V', 0x274C)));

$teams = array(
	'mystic' => TEAM_B,
	'valor' => TEAM_R,
	'instinct' => TEAM_Y,
	'unknown' => TEAM_UNKNOWN,
	'cancel' => TEAM_CANCEL,
);

$localizableStrings = [
	'Arrived1' => [ 'en' => 'Arrived', 'ru' => 'Пришёл' ],
	'Done' => [ 'ru' => 'Закончил' ],
	'Won\'t come' => [ 'ru' => 'Не приду' ],	
	'Raid Finished' => [ 'ru' => 'Рейд закончился' ],
	'Share' => [ 'ru' => 'Поделиться' ],
	'egg' => [ 'ru' => EMOJI_EGG, 'en'=>EMOJI_EGG ],
	'boss' => [ 'ru' => EMOJI_BOSS, 'en' => EMOJI_BOSS ],
	'How much time is left for Raid?' => [ 'ru' => 'Сколько осталось времени?' ],
	'Skip' => [ 'ru' => 'Пропустить' ],
	'Choose nearest Gym:' => [ 'ru' => 'Ближайший зал' ],
	'Choose Raid level:' => [ 'ru' => 'Уровень рейда' ],
	'Raid created' => [ 'ru' => 'Рейд создан' ],
	'Gym' => [ 'ru' => 'Зал' ],
	'Raid' => [ 'ru' => 'Рейд' ],
	' left' => [ 'ru' => ' осталось' ],
	'until' => [ 'ru' => 'до' ],
	'Ending' => [ 'ru' => 'Заканчивается' ],
	'\a\t' => [ 'ru' => '\в' ],
	'Location' => [ 'ru' => 'Место' ],
	'No participants yet.' => [ 'ru' => 'Пока никто не заявился.' ],
	'Updated' => [ 'ru' => 'Обновлено' ],
	' No participants' => [ 'ru' => ' Нет участников' ],
	'Waiting' => [ 'ru' => 'Ждём' ],
	'Arrived2' => [ 'en' => 'Arrived', 'ru' => 'Пришли' ],
	'Canceled' => [ 'ru' => 'Отменили' ],
	'[cancel] ' => [ 'ru' => '[отменил] ' ],
	'Complete' => [ 'ru' => 'Закончили' ],
	'Chat "%s" added to send list. Type /chats to manage.' => [
		'ru' =>
			'Чат "%s" добавлен в список рассылки. Напишите /chats для управления.'.CR.
			'НЕ ЗАБУДЬТЕ удалить после себя сообщение /post'.BOT_NAME.' чтобы не захламлять чат!'
	],
	'<b>Chat list</b>' => [ 'ru' => '<b>Список чатов</b>' ],
	'Post to %s chats' => [ 'ru' => 'Опубликовать в %s чат(а,ов)' ],
	'Raid has been published' =>  [ 'ru' => 'Рейд опубликован' ],
	'Message sent to chat' => [ 'ru' => 'Сообщение отправлено в чат' ],
	'common help' => [
		'en' => 'translation needed',
		'ru' => CR.
			'В переписке с ботом:'.CR.
			'● кинуть ГЕОЛОКАЦИЮ рядом с жимом, выбрать босса и время;'.CR.
			'● /level 35 — установить уровень игрока (достаточно сделать один раз);'.CR.
			'● /post'.BOT_NAME.' — зарегистриовать чат в списке рассылки;'.CR.
			'● /chats — управление списком чатов;'.CR.
			'All credits goes to @RaidPokemonBot, I just added several features I needed'
		],
	'chats help' => [
		'en' => 
			'translation needed'
		,
		'ru' => CR.
			'Если вы хотие постить рейды в чат одной кнопкой,'.CR.
			'нужно ОДИН РАЗ в каждом чате сказать боту /post'.BOT_NAME.CR.
			'С этого момента после создания рейда будет появляться список чатов.'.CR.
			'Для управления списком чатов используйте команду /chats в ЛИЧНОЙ переписке с ботом'.CR.
			'НЕ ЗАБУДЬТЕ удалить после себя сообщение /post'.BOT_NAME.' из чатов, чтобы не захламлять их!'
	],
	'I\'m late for 5 min' =>  ['ru' => 'Опаздываю на 5 минут'],
	'Won\'t come' =>  ['ru' => 'Не приду'],
	'You attendace was cancelled' => ['ru' => 'Вы отменили своё участие в рейде'],
	'You attendace was shifted by %d min' => ['ru' => 'Вы отложили вашу явку на %d минут'],
	'Already here!' => ['ru' => 'Да пришёл я, пришёл!'],
	'You arrived' => ['ru' => 'Вы пришли'],
	'Raid %s at "%s" awaits you in %d min' => ['ru' => 'Явитесь на рейд %s в зале «%s» через %d минут'],
	'You\'re late for the raid %s at "%s"' => ['ru' => 'Вы опаздываете на рейд %s в зале «%s»'],
	'You\'ve registered to the raid %s at "%s" in %d min' => ['ru' => 'Вы зарегистрировались на рейд %s в зале «%s» через %d минут'],
	'I\'m done!' => ['ru' => 'Я всё'],
	'Are you done with the raid %s at "%s"' => ['ru' => 'Вы закончили рейд %s в зале «%s»?'],

];
