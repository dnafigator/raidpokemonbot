INSERT INTO gyms (name, lat, lon, lat_str, lon_str) VALUES
('Паровоз', 51.728854, 39.271102, '51.728854', '39.271102'),
('Солнечное Граффити', 51.69858, 39.267658, '51.69858', '39.267658'),
('Вазы', 51.729459, 39.226893, '51.729459', '39.226893'),
('Храм святой мученицы Татьяны', 51.714102, 39.230966, '51.714102', '39.230966'),
('', 51.695971, 39.269832, '51.695971', '39.269832'),
('Ильич,  охраняющий гаражи', 51.696726, 39.260628, '51.696726', '39.260628'),
('Памятник Трактору', 51.715148, 39.226991, '51.715148', '39.226991'),
('Детская Площадка', 51.716834, 39.225738, '51.716834', '39.225738'),
('Kosmodemyanskaya Plaque', 51.694621, 39.266417, '51.694621', '39.266417'),
('ВГАУ имени императора Петра Великого', 51.712845, 39.226645, '51.712845', '39.226645'),
('Памятник К. Д. Глинке', 51.711745, 39.227415, '51.711745', '39.227415'),
('Площадка Пляжная', 51.693146, 39.280747, '51.693146', '39.280747'),
('Food Picture', 51.715641, 39.222901, '51.715641', '39.222901'),
('Летучий Голландец', 51.694477, 39.256273, '51.694477', '39.256273'),
('Героям Советского Союза', 51.692234, 39.267941, '51.692234', '39.267941'),
('Водонапорная Башня', 51.723896, 39.218861, '51.723896', '39.218861'),
('Flower Garden Bear', 51.719446, 39.219487, '51.719446', '39.219487'),
('Телефонная Будка', 51.690664, 39.260577, '51.690664', '39.260577'),
('Башня ', 51.688271, 39.279197, '51.688271', '39.279197'),
('Дерево Сов', 51.687889, 39.280806, '51.687889', '39.280806'),
('Храм вознесения Господня', 51.699444, 39.230301, '51.699444', '39.230301'),
('Дом С Корабликом', 51.688387, 39.256242, '51.688387', '39.256242'),
('Пушечка', 51.686861, 39.263691, '51.686861', '39.263691'),
('Бабочка-песочница', 51.726986, 39.209238, '51.726986', '39.209238'),
('Гипермаркет "Линия"', 51.686853, 39.257024, '51.686853', '39.257024'),
('Железнодорожный Район', 51.685645, 39.264103, '51.685645', '39.264103'),
('Висячие сады 1', 51.685674, 39.262292, '51.685674', '39.262292'),
('Старый маяк', 51.685781, 39.251708, '51.685781', '39.251708'),
('Ленин у ОДКБ № 2', 51.695991, 39.198397, '51.695991', '39.198397'),
('Братская могила 5', 51.700663, 39.198055, '51.700663', '39.198055'),
('Чижиков Ф.В.', 51.698915, 39.189167, '51.698915', '39.189167'),
('Ротонда', 51.693641, 39.208772, '51.693641', '39.208772'),
('Граффити на Детской площадке', 51.68911, 39.199472, '51.68911', '39.199472'),
('Граффити Unbone "And"', 51.703019, 39.193146, '51.703019', '39.193146'),
('Хользунов Виктор Степанович', 51.702828, 39.188328, '51.702828', '39.188328'),
('Медведь у управы', 51.693921, 39.182509, '51.693921', '39.182509'),
('Переход у автовокзала', 51.693113, 39.18282, '51.693113', '39.18282'),
('Мемориальные Доски #1', 51.698498, 39.182092, '51.698498', '39.182092'),
('Эйфелева башенка', 51.691094, 39.18362, '51.691094', '39.18362'),
('ВГУ / VGU', 51.704319, 39.186539, '51.704319', '39.186539'),
('Корабль На Стене', 51.702549, 39.182455, '51.702549', '39.182455'),
('Факультет Журналистики', 51.705988, 39.186205, '51.705988', '39.186205'),
('Братская могила в парке Динамо', 51.698664, 39.217999, '51.698664', '39.217999'),
('Monument at mass grave #3', 51.699275, 39.178882, '51.699275', '39.178882'),
('Дом Пионеров И Школьников', 51.685301, 39.188025, '51.685301', '39.188025'),
('В Память Коллективу Завода Коминтерна', 51.68822, 39.182319, '51.68822', '39.182319'),
('Мегаплощадка #5', 51.697852, 39.219937, '51.697852', '39.219937'),
(' Playground', 51.69712, 39.220709, '51.69712', '39.220709'),
('Даже В Старые Времена', 51.706055, 39.182025, '51.706055', '39.182025'),
('Dinamo', 51.693694, 39.221067, '51.693694', '39.221067'),
('Ленин у ВГТУ / Lenin', 51.68438, 39.18518, '51.68438', '39.18518'),
('Platonov Memorial Desk', 51.681655, 39.198775, '51.681655', '39.198775'),
('Старый аэропорт', 51.702605, 39.176548, '51.702605', '39.176548'),
('Памятник павшим смертью храбрых', 51.691494, 39.221512, '51.691494', '39.221512'),
('ВГПУ', 51.686047, 39.216791, '51.686047', '39.216791'),
('Их именами названы улицы', 51.683457, 39.184951, '51.683457', '39.184951'),
('Граффити попугая', 51.711439, 39.192807, '51.711439', '39.192807'),
('Медведь-Папа / Father-Bear', 51.683735, 39.183742, '51.683735', '39.183742'),
('Памятник "Отдавших Жизнь За Родину - Помните!"', 51.6885, 39.176212, '51.6885', '39.176212'),
('Граффити у глазной клиники', 51.711814, 39.190871, '51.711814', '39.190871'),
('МИИТ (Воронежский филиал)', 51.680513, 39.192599, '51.680513', '39.192599'),
('Братская могила у Ботанического сада', 51.712463, 39.204299, '51.712463', '39.204299'),
('У истоков Воронежского телевидения', 51.682002, 39.184879, '51.682002', '39.184879'),
('ЖК Арка', 51.710614, 39.183501, '51.710614', '39.183501'),
('Lion of VIVT', 51.684132, 39.217946, '51.684132', '39.217946'),
('Барельеф Пожарные', 51.683074, 39.216257, '51.683074', '39.216257'),
('Памятник паровозу', 51.68, 39.208134, '51.68', '39.208134'),
('Фонтан Олимпийский', 51.713834, 39.194499, '51.713834', '39.194499'),
('Сова', 51.706315, 39.174545, '51.706315', '39.174545'),
('Хорошая девочка Лида', 51.702988, 39.170756, '51.702988', '39.170756'),
('Только Факел!', 51.713385, 39.186883, '51.713385', '39.186883'),
('Памятник И. Д. Черняховскому', 51.678669, 39.206919, '51.678669', '39.206919'),
('Доска Лизюкову (старая)', 51.706291, 39.173351, '51.706291', '39.173351'),
('знАк', 51.681323, 39.180503, '51.681323', '39.180503'),
('Котенок с улицы Лизюкова', 51.706843, 39.172326, '51.706843', '39.172326'),
('Храм Всецарица', 51.714454, 39.184815, '51.714454', '39.184815'),
('Monument to the Voronezh State Academy of Medicine', 51.67669, 39.20485, '51.67669', '39.20485'),
('Мегион Фонтан', 51.679507, 39.18018, '51.679507', '39.18018'),
('Grape Panno', 51.712432, 39.178199, '51.712432', '39.178199'),
('Памятная Доска Ренц М. П. ', 51.676799, 39.209824, '51.676799', '39.209824'),
('Цапли,мельница и колодец', 51.693575, 39.164771, '51.693575', '39.164771'),
('Станция Плехановская', 51.676779, 39.185716, '51.676779', '39.185716'),
('Memorial ', 51.683819, 39.226098, '51.683819', '39.226098'),
('Благовещенский собор', 51.676312, 39.20994, '51.676312', '39.20994'),
('Дом Мягкова Ивана Саввича', 51.678822, 39.219192, '51.678822', '39.219192'),
('Слава советской науке', 51.675515, 39.209245, '51.675515', '39.209245'),
('Табличка Троепольского', 51.674673, 39.203472, '51.674673', '39.203472'),
('Памятный знак в честь 60-летия', 51.712697, 39.174233, '51.712697', '39.174233'),
('Пробитая стена', 51.67945, 39.174317, '51.67945', '39.174317'),
('Пчелка с буром в животе ', 51.704999, 39.164642, '51.704999', '39.164642'),
('ДС Юбилейный / Yubileinyi Spor', 51.673672, 39.193856, '51.673672', '39.193856'),
('Sofa Mural', 51.713822, 39.173728, '51.713822', '39.173728'),
('Памятник медработникам Воронежа', 51.674291, 39.185939, '51.674291', '39.185939'),
('Памятник Петру Первому', 51.673797, 39.211642, '51.673797', '39.211642'),
('Мухоморчик', 51.704032, 39.161611, '51.704032', '39.161611'),
('Переход у ТРЦ МП', 51.718014, 39.180254, '51.718014', '39.180254'),
('Атлант', 51.672194, 39.192285, '51.672194', '39.192285'),
('Памяти Курска', 51.713432, 39.169301, '51.713432', '39.169301'),
('Пресс', 51.68642, 39.161493, '51.68642', '39.161493'),
('Памятник А. П. Платонову', 51.672101, 39.209239, '51.672101', '39.209239'),
('Стул из ада', 51.673282, 39.182102, '51.673282', '39.182102'),
('Памятник первому десанту в СССР', 51.706808, 39.160876, '51.706808', '39.160876'),
('Дом Прав Человека', 51.673424, 39.21682, '51.673424', '39.21682'),
('Troitsky Plaque', 51.670805, 39.204859, '51.670805', '39.204859'),
('232 Стрелковой Дивизии', 51.676947, 39.170906, '51.676947', '39.170906'),
('Храм Блаженной Ксении Петербур', 51.717037, 39.172909, '51.717037', '39.172909'),
('Петух на заборе', 51.698335, 39.1566, '51.698335', '39.1566'),
('Академия Искусств', 51.706454, 39.159648, '51.706454', '39.159648'),
('БТР', 51.68937, 39.157716, '51.68937', '39.157716'),
('Graffiti on the Wall', 51.672648, 39.216706, '51.672648', '39.216706'),
('Свадебное древо в парке Победы', 51.709202, 39.161255, '51.709202', '39.161255'),
('Акатов монастырь', 51.674711, 39.222952, '51.674711', '39.222952'),
('Железные Листья', 51.670412, 39.189946, '51.670412', '39.189946'),
('Площадь Победы', 51.670586, 39.210359, '51.670586', '39.210359'),
('White Turtle Statue', 51.718106, 39.172714, '51.718106', '39.172714'),
('Яблоко Мира', 51.712476, 39.163207, '51.712476', '39.163207'),
('Вгу Корпус #10', 51.668998, 39.201872, '51.668998', '39.201872'),
('Курский вокзал', 51.671223, 39.177247, '51.671223', '39.177247'),
('Play Structure', 51.703307, 39.153203, '51.703307', '39.153203'),
('Пивоваренный завод «Балтика»', 51.672024, 39.155399, '51.672024', '39.155399'),
('Chunosov Plaque ', 51.672977, 39.153777, '51.672977', '39.153777'),
('Солнышко ', 51.676348, 39.15302, '51.676348', '39.15302'),
('Памятная Таблица Героям Бреста ', 51.674516, 39.1627, '51.674516', '39.1627'),
('Слон', 51.676199, 39.149021, '51.676199', '39.149021'),
('Белоснежка и гномы', 51.670579, 39.164611, '51.670579', '39.164611'),
('Розовое Граффити', 51.677713, 39.160968, '51.677713', '39.160968'),
('Твори', 51.670179, 39.165254, '51.670179', '39.165254'),
('Ленин', 51.676984, 39.14773, '51.676984', '39.14773'),
('Школьный Автобус', 51.67914, 39.158494, '51.67914', '39.158494'),
('Фонтан ГБУ ВО ОЦРДО', 51.674896, 39.14496, '51.674896', '39.14496'),
('Railway Creative', 51.665447, 39.152956, '51.665447', '39.152956'),
('Корова Просто Космос', 51.661128, 39.153838, '51.661128', '39.153838'),
('Indiana Jones', 51.65962, 39.154339, '51.65962', '39.154339'),
('Доска Керамического Завода', 51.658891, 39.154987, '51.658891', '39.154987'),
('Детское творчество на трансформаторе', 51.678013, 39.133089, '51.678013', '39.133089'),
('Graffity Man', 51.678712, 39.13249, '51.678712', '39.13249'),
('Карбышев Д. М.', 51.656262, 39.152125, '51.656262', '39.152125'),
('Золотоискатель', 51.66458, 39.180372, '51.66458', '39.180372'),
('Маленькая Колокольня', 51.656888, 39.142875, '51.656888', '39.142875'),
('Ret', 51.654713, 39.159997, '51.654713', '39.159997'),
('Дом 18 Века', 51.665392, 39.185165, '51.665392', '39.185165'),
('Баскетбольная Площадка', 51.659647, 39.130309, '51.659647', '39.130309'),
('Гена и Чебурашка', 51.662662, 39.18603, '51.662662', '39.18603'),
('Храм во имя Всех святых, в земле Русской просиявших', 51.651437, 39.150446, '51.651437', '39.150446'),
('Стела "Механический завод"', 51.655292, 39.178132, '51.655292', '39.178132'),
('Завтра', 51.651398, 39.167643, '51.651398', '39.167643'),
('Сказка "Теремок"', 51.686238, 39.127387, '51.686238', '39.127387'),
('Старый Фонтан', 51.650431, 39.149887, '51.650431', '39.149887'),
('Железная Кошка', 51.664252, 39.189729, '51.664252', '39.189729'),
('Фонтан на рынке', 51.6827, 39.123354, '51.6827', '39.123354'),
('БЦ ГЧ / Chizhov Gallery Business Center', 51.667205, 39.191649, '51.667205', '39.191649'),
('Дом со шпилем', 51.658067, 39.184387, '51.658067', '39.184387'),
('Дворец Культуры Имени 50 Летия Октября', 51.650493, 39.165949, '51.650493', '39.165949'),
('Самолет на Ворошилова', 51.65018, 39.164322, '51.65018', '39.164322'),
('Эй Пёс', 51.651525, 39.139531, '51.651525', '39.139531'),
('Памятная доска В. Н. Костину', 51.655007, 39.130903, '51.655007', '39.130903'),
('Voronezh Post Office ', 51.649125, 39.160907, '51.649125', '39.160907'),
('Литтл-Бигбен', 51.64899, 39.158829, '51.64899', '39.158829'),
('Харчевня', 51.65027, 39.143082, '51.65027', '39.143082'),
('Танцы на костях', 51.655395, 39.182347, '51.655395', '39.182347'),
('Царевна Лебедь', 51.64897, 39.151582, '51.64897', '39.151582'),
('Library 15', 51.654979, 39.129773, '51.654979', '39.129773'),
('Small Tank', 51.666053, 39.193202, '51.666053', '39.193202'),
('Краеведческий музей', 51.666724, 39.193575, '51.666724', '39.193575'),
('Путь Коровы', 51.656874, 39.185696, '51.656874', '39.185696'),
('Водонапорная Башня На Беговой ', 51.694552, 39.139628, '51.694552', '39.139628'),
('Бутылочная пальма', 51.649797, 39.140874, '51.649797', '39.140874'),
('Памятник граммофону', 51.651538, 39.135092, '51.651538', '39.135092'),
('Почта #394062', 51.657276, 39.124613, '51.657276', '39.124613'),
('Цирк / Circus', 51.655854, 39.185557, '51.655854', '39.185557'),
('Geometric Wall', 51.649974, 39.175699, '51.649974', '39.175699'),
('Афанасьев Василий Николаевич', 51.665534, 39.195955, '51.665534', '39.195955'),
('repka mural', 51.698436, 39.147831, '51.698436', '39.147831'),
('Графити Обнаженная', 51.662143, 39.195573, '51.662143', '39.195573'),
('Красная Вилка-Ложка', 51.655896, 39.190591, '51.655896', '39.190591'),
('Детская Библиотека', 51.651001, 39.128504, '51.651001', '39.128504'),
('Bull Statue', 51.645416, 39.147262, '51.645416', '39.147262'),
('Царевна лягушка', 51.652708, 39.1237, '51.652708', '39.1237'),
('Gun', 51.69738, 39.134093, '51.69738', '39.134093'),
('Еврейское кладбище', 51.645741, 39.170648, '51.645741', '39.170648'),
('Церковь Андрея Первозванного', 51.700935, 39.151939, '51.700935', '39.151939'),
('Корова', 51.645766, 39.139653, '51.645766', '39.139653'),
('Der Hase', 51.659435, 39.114863, '51.659435', '39.114863'),
('Памятник А. С. Пушкину', 51.66178, 39.199235, '51.66178', '39.199235'),
('Театр оперы и балета.', 51.66131, 39.199101, '51.66131', '39.199101'),
('Памятник В. С. Высоцкому', 51.666764, 39.201823, '51.666764', '39.201823'),
('Chico', 51.65442, 39.192611, '51.65442', '39.192611'),
('01пожар8', 51.644102, 39.146079, '51.644102', '39.146079'),
('Clock Tower ', 51.661817, 39.200306, '51.661817', '39.200306'),
('Красная Конница Шендрикова', 51.65779, 39.110701, '51.65779', '39.110701'),
('Карта', 51.658358, 39.105509, '51.658358', '39.105509'),
('Грустный Поросенок', 51.657851, 39.104945, '51.657851', '39.104945'),
('Памяти Любы Шевцовой', 51.658091, 39.103978, '51.658091', '39.103978'),
('Карлсон Художник', 51.654629, 39.112142, '51.654629', '39.112142'),
('Цветок', 51.653303, 39.117369, '51.653303', '39.117369'),
('Дверь В Лето', 51.651942, 39.101567, '51.651942', '39.101567'),
('Спорт', 51.651469, 39.102712, '51.651469', '39.102712'),
('Машинко', 51.648279, 39.122785, '51.648279', '39.122785'),
('Сгоревший ресторан Сосновый Бор', 51.648053, 39.11746, '51.648053', '39.11746'),
('Змий', 51.650968, 39.10069, '51.650968', '39.10069'),
('Танк(Т-34-85)', 51.646678, 39.123601, '51.646678', '39.123601'),
('Парма', 51.64659, 39.127611, '51.64659', '39.127611'),
('Курский фонтан', 51.646591, 39.128041, '51.646591', '39.128041'),
('Bears Sculpture', 51.649797, 39.10039, '51.649797', '39.10039'),
('Разрушенная Колонна', 51.647028, 39.110246, '51.647028', '39.110246'),
('Избушка', 51.660319, 39.085376, '51.660319', '39.085376'),
('Люди Козы', 51.645539, 39.109944, '51.645539', '39.109944'),
('Часовня МВД', 51.646705, 39.101923, '51.646705', '39.101923'),
('Золотой орёл', 51.644816, 39.137686, '51.644816', '39.137686'),
('Газебо', 51.643263, 39.145247, '51.643263', '39.145247'),
('Граффити Моника Белуччи', 51.640603, 39.153314, '51.640603', '39.153314'),
('Джунгли', 51.706115, 39.136199, '51.706115', '39.136199'),
('Аист приносящий панду', 51.706593, 39.138545, '51.706593', '39.138545'),
('Огромная Детская Развлекуха', 51.70619, 39.146051, '51.70619', '39.146051'),
('Fantastic Wall-ART', 51.6486, 39.191639, '51.6486', '39.191639'),
('Бассеин ВГАСУ', 51.649567, 39.193628, '51.649567', '39.193628'),
('Граффити Космонавт', 51.650683, 39.199487, '51.650683', '39.199487'),
('Митрофановский источник', 51.653267, 39.209385, '51.653267', '39.209385'),
('Памятник Святителю Митрофану', 51.656293, 39.204882, '51.656293', '39.204882'),
('Студентам И сотрудниками павшим в боях за  родину', 51.656486, 39.204474, '51.656486', '39.204474'),
('Astronauts Graffiti', 51.658411, 39.198372, '51.658411', '39.198372'),
('Памятник В. И. Ленину', 51.66069, 39.199988, '51.66069', '39.199988'),
('Gudwin', 51.659433, 39.207856, '51.659433', '39.207856'),
('Цветной фонтан', 51.662047, 39.202003, '51.662047', '39.202003'),
('Корабль-музей " Гото Предестинация"', 51.656043, 39.215999, '51.656043', '39.215999'),
('Каменный мост', 51.661201, 39.207289, '51.661201', '39.207289'),
('Ship', 51.659606, 39.218304, '51.659606', '39.218304'),
('Белый Бим Чёрное Ухо', 51.666104, 39.205434, '51.666104', '39.205434'),
('В. И. Ленин у ТЭЦ-1', 51.62959, 39.226547, '51.62959', '39.226547'),
('Monument to the Voronezh poet Koltsov', 51.665834, 39.21098, '51.665834', '39.21098'),
('Обсерватория', 51.66401, 39.216882, '51.66401', '39.216882'),
('ТЭЦ-1 ', 51.631496, 39.229548, '51.631496', '39.229548'),
('Центральный телеграф / Central Telegraph', 51.667722, 39.207235, '51.667722', '39.207235'),
('Как Всегда ', 51.633111, 39.230821, '51.633111', '39.230821'),
('Здесь не прошёл враг', 51.632244, 39.23059, '51.632244', '39.23059'),
('Силуэт города', 51.637323, 39.234011, '51.637323', '39.234011'),
('4 Фонтана', 51.665507, 39.219511, '51.665507', '39.219511'),
('Недостроенный Корабль', 51.666193, 39.219371, '51.666193', '39.219371'),
('Часовня Успенской церкви', 51.646072, 39.238003, '51.646072', '39.238003'),
('Летательный Аппарат', 51.638095, 39.239798, '51.638095', '39.239798'),
('фонтан', 51.638552, 39.241096, '51.638552', '39.241096'),
('Кот-Учёный', 51.642004, 39.241958, '51.642004', '39.241958'),
('Фонтан Парк авиастроителей', 51.640839, 39.242311, '51.640839', '39.242311'),
('Алые Паруса Главный Вход', 51.657464, 39.236802, '51.657464', '39.236802'),
('Mikhaylov Plaque', 51.635449, 39.243896, '51.635449', '39.243896'),
('Памятник штурмовику Ил-2', 51.636592, 39.247399, '51.636592', '39.247399'),
('Стелла на ВАСО', 51.637147, 39.251742, '51.637147', '39.251742'),
('Воронежская духовная семинария', 51.656258, 39.243797, '51.656258', '39.243797'),
('Стелла В Парке Патриотов', 51.659404, 39.246892, '51.659404', '39.246892'),
('Museum - A Memorial 1941-1945 (МИ-8)', 51.659885, 39.246664, '51.659885', '39.246664'),
('Корабль у Чернавского моста', 51.665275, 39.244708, '51.665275', '39.244708'),
('Памятная Доска Суйтс К.Н.', 51.666717, 39.243549, '51.666717', '39.243549'),
('Two Bikers Graffiti', 51.66401, 39.250229, '51.66401', '39.250229'),
('Авто Фак', 51.664834, 39.276871, '51.664834', '39.276871'),
('Почта России 394007', 51.668032, 39.254028, '51.668032', '39.254028'),
('Прицеп на подставке ', 51.669761, 39.259851, '51.669761', '39.259851'),
('Fairy', 51.670495, 39.249181, '51.670495', '39.249181'),
('Детская площадка', 51.673709, 39.255647, '51.673709', '39.255647'),
('Фонтан Завода Электроника', 51.676957, 39.257323, '51.676957', '39.257323'),
('Плац', 51.675236, 39.247629, '51.675236', '39.247629'),
('Графити"Митол!"', 51.673116, 39.238511, '51.673116', '39.238511'),
('Monument of Soviet Science', 51.680333, 39.261441, '51.680333', '39.261441'),
('Солнышко', 51.680609, 39.264697, '51.680609', '39.264697'),
('Ленин-летун как Гагарин ', 51.676677, 39.244077, '51.676677', '39.244077'),
('ВЭПИ', 51.680108, 39.255226, '51.680108', '39.255226'),
('Озеро на Минской', 51.681667, 39.260254, '51.681667', '39.260254'),
('Страшный Пень! ', 51.716523, 39.274765, '51.716523', '39.274765'),
('Николай Чудотворец', 51.723504, 39.267424, '51.723504', '39.267424'),
('Здание железной дороги (Хмельницкого 13)', 51.725263, 39.262422, '51.725263', '39.262422'),
('Качели', 51.725913, 39.190369, '51.725913', '39.190369'),
('Братская могила #16', 51.730368, 39.196796, '51.730368', '39.196796'),
('Павшим сотрудникам СХИ', 51.72781, 39.198805, '51.72781', '39.198805'),
('Памятник Лене Фроловой', 51.725848, 39.201016, '51.725848', '39.201016'),
('Crazy Frog', 51.724405, 39.176471, '51.724405', '39.176471'),
('Mill', 51.723253, 39.177608, '51.723253', '39.177608'),
('Винни Пух', 51.722192, 39.177375, '51.722192', '39.177375'),
('Giraffe', 51.719935, 39.17116, '51.719935', '39.17116'),
('Друзья с конфетой', 51.719536, 39.165391, '51.719536', '39.165391'),
('Iron Heart', 51.718766, 39.159311, '51.718766', '39.159311'),
('Забавный Пень', 51.721662, 39.15582, '51.721662', '39.15582'),
('Сиреневый Попугай', 51.71804, 39.157671, '51.71804', '39.157671'),
('СОК Олимпик / Olympic Sports Complex', 51.753008, 39.18717, '51.753008', '39.18717'),
('Домик Бабы Яги', 51.753543, 39.189366, '51.753543', '39.189366'),
('Мемориал в Подгорном', 51.732727, 39.149716, '51.732727', '39.149716'),
('Галактика', 51.713961, 39.157065, '51.713961', '39.157065'),
('Победа.Pobeda', 51.711697, 39.152124, '51.711697', '39.152124'),
('Каменная композиция', 51.711592, 39.151446, '51.711592', '39.151446'),
('Торговка на Невском', 51.708461, 39.149191, '51.708461', '39.149191'),
('Граффити Русалочка', 51.710096, 39.142683, '51.710096', '39.142683');

